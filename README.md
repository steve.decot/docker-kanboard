# docker-kanboard
## Description
Kanboard est une application logicielle open source de gestion de projet qui utilise un tableau Kanban pour mettre en œuvre le système de gestion de processus Kanban.

## Deploy

Affectez les variables selon vos besoins ( ${VAR}) 
La stack peut fonctionner avec Traefik, les labels sont présents dans le compose. 


## Author

Steve Decot
